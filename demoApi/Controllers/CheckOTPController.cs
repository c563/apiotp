using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using demoApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace demoApi.Controllers
{
    [Route("api/checkOTP")]
    [ApiController]
    public class CheckOTPController : ControllerBase
    {
        // GET: api/Login
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Login/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        static string sha256(string randomString)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }

        static DateTime getTime()
        {
            DateTime utcDateTime = DateTime.UtcNow;
            string vnTimeZoneKey = "SE Asia Standard Time";
            TimeZoneInfo vnTimeZone = TimeZoneInfo.FindSystemTimeZoneById(vnTimeZoneKey);
            DateTime ngayhientai = DateTime.Parse(TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, vnTimeZone).ToShortDateString());
            DateTime ngaygiohientai = TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, vnTimeZone);
            return ngaygiohientai;
        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        // POST: api/Login
        [HttpPost]
        public String Post([FromBody] PostOTP data)
        {
            //2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824
            String otp = data.OTP;
            String userID = data.userID;

            DateTime foo = getTime();
            long timeServer = ((DateTimeOffset)foo).ToUnixTimeSeconds();

            //if (timeServer - data.time > 60)
            //{
            //    return "Mã OTP của bạn đã hết hạn!";
            //}
            if (userID == "KMA-AT15")
            {
                string res = "";
                bool checking = false;
                for (int i = 0; i <= 60; i++)
                {
                    long timeCheck = timeServer - i;
                    //long timeCheck = 5555555559 - i;
                    //timeCheck.ToString()
                    string otpHash = sha256(timeCheck.ToString() + "2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824");
                    string last_six_characters = otpHash.Substring(otpHash.Length - 6);
                    string a = Convert.ToInt32(last_six_characters, 16).ToString();
                    while (a.Length < 8)
                    {
                        a = "0" + a;
                    }
                    if (a.ToString() == data.OTP)
                    {
                        checking = true;
                        return "Mã OTP hợp lệ";
                    }
                    res += a.ToString() + "/";
                }
                return "Mã OTP không hợp lệ";
            }
            else
            {
                return "User không tồn tại";
            }


        }

        // PUT: api/Login/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Login/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
