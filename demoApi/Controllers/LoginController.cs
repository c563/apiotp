using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using demoApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace demoApi.Controllers
{
    [Route("api/login")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        // GET: api/Login
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        //[ResponseType(typeof(User))]
        public FormRes Post([FromBody] Login login)
        {
            FormRes res = new FormRes();
            if (String.Equals(login.userName, "user01"))
            {
                if (String.Equals(login.password, "123123"))
                {
                    res.message = "Đăng nhập thành công";
                    res.status = 1;
                    res.userID = "KMA-AT15";
                    return res;
                }
                else
                {
                    res.message = "Mật khẩu không chính xác";
                    res.status = 0;
                    return res;
                }
            }
            else
            {
                res.message = "Tài khoản không chính xác";
                res.status = 0;
                return res;
            }
        }
    }
}
